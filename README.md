## Project setup

```
npm install
```

## Running project

To run on default port 8080:

```
npm run serve
```

To run on a certain port (i.e. 3000):

```
npm run serve -- --port 3000
```

### Description

To create animations for sorting algorithms learnt at universities.

Created using Vue and D3 to show a visualised Bar Graph in real time. User can generate random number to expand how they can see the selected sorting algorithm works.

## Features Implemented:

- Bubble Sort
- Quick Sort
- Selection Sort
- Insertion Sort
- Merge Sort

## Features that wasn't implement:

- Indications between sorted and unsorted (can be via text)
- Tooltip message box styling

## Images

![Alt Text](./images/showcase.png)
