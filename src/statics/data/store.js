import Vue from "vue";
import { arraySwap, sleep } from "./sortFunctions";
import { select, selectAll } from "d3-selection";

export const store = Vue.observable({
  currentmp1: 0,
  currentmp2: 0,
  sleepTime: 300,
  barChartData: [
    {
      name: "Roses",
      amount: 25
    },
    {
      name: "Tulips",
      amount: 40
    },
    {
      name: "Daisies",
      amount: 15
    },
    {
      name: "Narcissuses",
      amount: 9
    },
    {
      name: "s",
      amount: 10
    }
  ],
  sortList: [
    {
      name: "BUBBLE SORT",
      algorithm: "Bubble Sort",
      fName: "bubbleSort",
      enabled: 1
    },
    {
      name: "QUICK SORT",
      algorithm: "Quick Sort",
      fName: "quickSort",
      enabled: 1
    },
    {
      name: "MERGE SORT",
      algorithm: "Merge Sort",
      fName: "",
      enabled: 1
    },
    {
      name: "SELECTION SORT",
      algorithm: "Selection Sort",
      fName: "",
      enabled: 1
    },
    {
      name: "INSERTION SORT",
      algorithm: "Insertion Sort",
      fName: "",
      enabled: 1
    }
  ],
  redrawToggled: true
});

export const mutations = {
  changeRedraw(tboolean) {
    store.redrawToggled = tboolean;
  },
  loadColor(n, color) {
    select("rect:nth-child(" + n + ")").attr("fill", color);
  },
  loadColorDefault(color) {
    selectAll("rect").attr("fill", color);
  },
  getCurrentIndex(pos1, pos2) {
    store.currentmp1 = pos1;
    store.currentmp2 = pos2;
  },
  setBarChartData(array) {
    store.barChartData = array;
  },
  updateData(array) {
    store.barChartData = array;
  },
  async bubbleSort(a, b) {
    var len = a.length;
    for (var i = len - 1; i >= 0; i--) {
      for (var j = 1; j <= i; j++) {
        if (a[j - 1] > a[j]) {
          this.swap(a, j - 1, j, b);
          await sleep(this.sleepTime / 5);
        }
        this.loadColorDefault("steelblue");
      }
    }
  },
  // Sets the indexes position color before swapping
  setIndexColor(index1, index2) {
    this.loadColor(index1, "cornflowerblue");
    this.loadColor(index2, "lightskyblue");
    this.getCurrentIndex(index1, index2);
  },
  // Will swap left and right indexes
  swap(items, leftIndex, rightIndex, items2) {
    this.setIndexColor(leftIndex, rightIndex);
    arraySwap(items, leftIndex, rightIndex, items2);
  },
  // Partition using a pivot
  partition(items, left, right, arrayB) {
    var pivot = items[Math.floor((right + left) / 2)],
      i = left,
      j = right;
    while (i <= j) {
      while (items[i] < pivot) {
        i++;
      }
      while (items[j] > pivot) {
        j--;
      }
      if (i <= j) {
        this.swap(items, i, j, arrayB);
        i++;
        j--;
      }
    }
    return i;
  },
  async quickSorte(items, left, right, arrayB) {
    var index;
    if (items.length > 1) {
      index = this.partition(items, left, right, arrayB);
      await sleep(this.sleepTime);
      if (left < index - 1) {
        await this.quickSorte(items, left, index - 1, arrayB);
      }
      if (index < right) {
        await this.quickSorte(items, index, right, arrayB);
      }
    }
  },
  // Main quick sort function
  quickSort(items, arrayB) {
    this.quickSorte(items, 0, items.length - 1, arrayB);
  },
  // Main insertion sort function
  async insertSort(array, arrayB) {
    for (var i = 0; i < array.length; i++) {
      let minIndex = i;

      for (var j = i; j < array.length; j++) {
        if (array[j] < array[minIndex]) {
          minIndex = j;
        }
      }
      this.swap(array, i, minIndex, arrayB);
      await sleep(this.sleepTime);
    }
  },
  // Main selection sort function
  async selectionSort(array, arrayB) {
    for (let i = 0; i < array.length; i++) {
      let min = i;
      for (let j = i + 1; j < array.length; j++) {
        if (array[min] > array[j]) {
          min = j;
        }
      }
      if (min !== i) {
        this.swap(array, i, min, arrayB);
      }
      await sleep(this.sleepTime);
    }
  },

  // Main merge algorithm
  // Make a copy and parse into merge sort function
  merge(arr1, arr2) {
    let copy = arr1.slice();
    let copy1 = arr2.slice();

    this.mergeSort(copy, 0, copy.length, copy1);
    return;
  },

  // Using divide and conquer technique to split at half point recursively
  // Each time when it compares that a[i] < b[i], swap 2 values
  async mergeSort(a, start, end, b) {
    if (end - start <= 1) return;

    var mid = Math.round((end + start) / 2);

    await this.mergeSort(a, start, mid, b);
    await this.mergeSort(a, mid, end, b);

    let i = start,
      j = mid;

    while (i < end && j < end) {
      if (a[i] > a[j]) {
        let t = a[j];
        let t1 = b[j];

        a.splice(j, 1);
        a.splice(i, 0, t);

        b.splice(j, 1);
        b.splice(i, 0, t1);

        store.barChartData.splice(j, 1);
        store.barChartData.splice(i, 0, t1);

        this.setIndexColor(i, j);
        j++;
      }
      i++;
      if (i == j) j++;

      await sleep(this.sleepTime);
    }
  }
};
