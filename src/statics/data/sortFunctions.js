// Global sleep async function
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Help to set an index value
function arraySetWithoutIndexes(array, index, value) {
  array.splice(index, 1, value);
}

// used by most sorting algorithm to swap left and right indexes
function arraySwap(array, indexA, indexB, b) {
  var x = array[indexA];
  var y = b[indexA];

  arraySetWithoutIndexes(array, indexA, array[indexB]);
  arraySetWithoutIndexes(array, indexB, x);

  arraySetWithoutIndexes(b, indexA, b[indexB]);
  arraySetWithoutIndexes(b, indexB, y);
}

module.exports = {
  arraySwap,
  sleep
};
